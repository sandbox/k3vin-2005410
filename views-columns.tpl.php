<?php
/**
 * @file
 * Template to display a view as a number of configurable columns.
 *
 * Available variables:
 * - $title : The title of this group of rows.  May be empty.
 * - $no_columns : The configured number of columns.
 * - $sort_order : Sort horizontal or vertical.
 * - $rows : The view results.
 * - $columns : An array containing the columns and data.
 *
 * @see template_preprocess_views_columns()
 * 
 * @ingroup views_templates
 * @ingroup themeable
 */
?>
<?php if (!empty($title)) : ?>
  <h3><?php print $title; ?></h3>
<?php endif; ?>

<div class="column-wrapper column-<?php print $no_columns;?>">
  <?php foreach ($columns as $id => $column): ?>
    <div <?php print drupal_attributes($column['attributes']); ?>>
      <div class="inner">
      <?php foreach ($column['data'] as $id => $row): ?>
        <?php print $row; ?>
      <?php endforeach; ?>
      </div>
    </div>
  <?php endforeach; ?>
</div>
