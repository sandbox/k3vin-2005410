How to use:

Enable to module, and edit a view. Under format choose 'columns'.
In the style optins you can configure the number of columns to show.

Additionally you can choose how the columns should be filled, horizontally or 
vertically.

Horizontally:
1 | 2 | 3
4 | 5 | 6

Vertically:
1 | 3 | 5
2 | 4 | 6

The module contains some basic CSS for the columns layout. Up to 10 columns are 
supported. If you need more then 10 coluns, you will have to write some 
additional CSS yourself. 
