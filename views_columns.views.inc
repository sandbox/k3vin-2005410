<?php
/**
 * @file
 * views_columns.views.inc
 * Contains the plugin that divides the output into a configurable number 
 * of columns.
 */

/**
 * Implements of hook_views_plugins().
 */
function views_columns_views_plugins() {
  return array(
    'style' => array(
      'columns' => array(
        'title' => t('Columns'),
        'help' => t('Display the output into a configurable number of columns.'),
        'handler' => 'ViewsColumnsPluginStyleColumns',
        'uses row plugin' => TRUE,
        'uses fields' => TRUE,
        'uses options' => TRUE,
        'type' => 'normal',
        'theme' => 'views_columns',
        'even empty' => TRUE,
      ),
    ),
  );
}
